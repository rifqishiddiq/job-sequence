let point = ["J1", "J2", "J3", "J4", "J5", "J6"]
let path = ["J1-J1", "J1-J2", "J1-J3", "J1-J4", "J1-J5", "J1-J6",
            "J2-J1", "J2-J2", "J2-J3", "J2-J4", "J2-J5", "J2-J6",
            "J3-J1", "J3-J2", "J3-J3", "J3-J4", "J3-J5", "J3-J6",
            "J4-J1", "J4-J2", "J4-J3", "J4-J4", "J4-J5", "J4-J6",
            "J5-J1", "J5-J2", "J5-J3", "J5-J4", "J5-J5", "J5-J6",
            "J6-J1", "J6-J2", "J6-J3", "J6-J4", "J6-J5", "J6-J6"]
let distance = ["0", "5", "3", "4", "2", "1",
                "2", "0", "1", "2", "3", "2",
                "2", "5", "0", "1", "2", "3",
                "1", "4", "4", "0", "1", "2",
                "1", "3", "4", "5", "0", "5",
                "4", "4", "2", "3", "1", "0"]

function calculate_permutation(param) {
    let count = 0
    let arr_path = []
    let arr_distance = []
    let arr_total = []
    
    // loop untill array-1 factorial (permutation)
    let total_loop = param.length-1;
    if (point.length > 3) {
        for (let i = point.length-2; i > 0; i--) {
            total_loop = total_loop*i
        }
    }

    //fungsi permutasi
    let permutation = (str, res) => {
        // if the path is ended
        if (str.length == 0) {
            let arr_final = []
            res.forEach(element => {
                arr_final.push(element)
            });
            // arr_final.push(point[0]) // comment line code ini apabila jalur tidak ingin kembali ke titik awal
    
            let arr_step = []
            let text = ""
            let total_distance = 0
            for (let i = 0; i < arr_final.length-1; i++) {
                text = arr_final[i] + "-" + arr_final[i+1]
                let index = path.findIndex(x => x.includes(text));
                arr_step.push(distance[index])
                total_distance += parseInt(distance[index])
            }
    
            // console.log("Sequence " + arr_final.join("-") + " dengan waktu " + arr_step.join("+") + " = " + total_distance)
            arr_path.push(arr_final)
            arr_distance.push(arr_step)
            arr_total.push(total_distance)
            count++
        }
        if (count < total_loop) {
            for (let i = 0; i < str.length; i++) {
                let arr = []
                let rest = []
                res.forEach(element => {
                    arr.push(element)
                });
                arr.push(str[i])
    
                for (let index = 0; index < i; index++) {
                    rest.push(str[index])
                }
                for (let index = i+1; index < str.length; index++) {
                    rest.push(str[index])
                }
    
                permutation(rest, arr)
            }
        }
    }

    permutation(param, [])

    let lowest_total = Math.min(...arr_total)
    
    let index_lowest = arr_total.indexOf(lowest_total)
    let lowest_path = arr_path[index_lowest].join("-")
    let lowest_distance = arr_distance[index_lowest].join(" + ")

    return [
        lowest_total,
        lowest_path,
        lowest_distance
    ]
}


let arr_lowest_total = []
let arr_lowest_path = []
let arr_lowest_distance = []
for (let index = 0; index < point.length; index++) {
    let index_dummy = 0
    let arr_temp = []

    // shift the value to next index
    for (let index2 = index; index2 < point.length+index; index2++) {
        index_dummy = index2
        if (index_dummy > point.length-1) {
            index_dummy-=point.length
        }
        arr_temp.push(point[index_dummy])
    }
    let result = calculate_permutation(arr_temp)
    arr_lowest_total.push(result[0])
    arr_lowest_path.push(result[1])
    arr_lowest_distance.push(result[2])
}

for (let index = 0; index < arr_lowest_total.length; index++) {
    console.log("Sequence terpendek dari " + point[index] + " adalah " + arr_lowest_path[index]
        + " dengan waktu " + arr_lowest_distance[index] + " = " + arr_lowest_total[index])
}

let lowest = Math.min(...arr_lowest_total)
let index_lowest = arr_lowest_total.indexOf(lowest)

console.log("\nSequence terpendek adalah " + arr_lowest_path[index_lowest]
    + "dengan waktu " + arr_lowest_distance[index_lowest] + " = " + lowest + "\n")
